import attr
from collections import deque
from typing import Tuple, MutableMapping, Optional, Any, Dict, Union, Type


def _split_head(key: str, delimiter: str) -> Tuple[str, Optional[str]]:
    """
    Split a string on first delimiter.

    Args:
        key: a string, optionally including a delimiter
        delimiter: string to use as split.

    Returns:
        a Tuple as head, rest

    """

    key = key.split(delimiter, maxsplit=1)

    if len(key) == 1:
        return key[0], None

    return key[0], key[1]


@attr.s(auto_attribs=True)
class Node(MutableMapping):
    children: Dict[str, "Node"] = attr.Factory(dict)
    """subnodes of this node"""

    value: Optional[Any] = None
    """stored value"""

    delimiter: str = "."
    """delimiter to use for parsing submappings"""

    node_factory: Optional[Type["Node"]] = None
    """factory to use to create children"""

    def __getitem__(self, key: str) -> Any:
        """
        Retrieves a key, splitting recursively on a delimiter.

        Args:
            key: the stored key to return

        Returns:
            a value if the key exists in the collection, or raises a KeyError if not.
        """
        # split key based on delimiter
        head, rest = _split_head(key, self.delimiter)

        if head not in self.children:
            raise KeyError(f"{key} not found")

        child = self.children[head]

        return child.value if rest is None else child[rest]

    def __contains__(self, key: str):
        head, rest = _split_head(key, self.delimiter)

        if head not in self.children:
            return False

        if rest is None:
            return True

        return rest in self.children[head]

    def _make_child(self, *args, **kwargs):
        if self.node_factory is None:
            return Node(*args, **kwargs)

        return self.node_factory(*args, **kwargs)

    def __setitem__(self, key, value):
        head, rest = _split_head(key, self.delimiter)

        if head not in self.children:  # new branch
            node = self._make_child(delimiter=self.delimiter)

            if rest is None:
                node.value = value
            else:
                node[rest] = value

            self.children[head] = node
        else:  # existing branch
            child = self.children[head]
            if rest is None:
                child.value = value
            else:
                if child.value is not None:
                    child[""] = child.value
                    child.value = None

                child[rest] = value

    def __delitem__(self, key):
        head, rest = _split_head(key, self.delimiter)

        if rest is None:  # simply delete the child
            del self.children[head]
            return

        # remove recursively
        child = self.children[head]
        del child[rest]

    def __iter__(self):
        # iterate breadth-first
        self._iter_queue = deque([(k, v) for k, v in self.children.items()])
        return self

    def __next__(self):
        while True:
            if len(self._iter_queue) == 0:
                raise StopIteration()

            name, node = self._iter_queue.pop()

            for child, value in node.children.items():
                self._iter_queue.append((self.delimiter.join([name, child]), value))

            if node.value is not None:
                break

        return name

    def __len__(self):
        sum = 0

        for name, child in self.children.items():
            sum += len(child)

        if len(self.children) == 0:
            sum += 1
        return sum

    def asdict(self) -> Dict[str, Union[Dict, Any]]:
        """
        Return the data contained in this structure as a nested dictionary.

        Returns:
            a nested dict of dict structure containing values stored in this structure.
        """
        d = {}
        for name, node in self.children.items():
            if len(node.children) > 0:
                d.update({name: node.asdict()})
            else:
                d.update({name: node.value})

        return d
