import logging
import attr
import blinker
from typing import Tuple, MutableMapping, Optional, Any, Dict, Union, Sequence, Callable
from abc import ABC, abstractmethod
from collections import namedtuple
from .trie import Node

log = logging.getLogger(__name__)

_callback_namespace = blinker.Namespace()

DummyLoop = namedtuple("DummyLoop", ["name", "state"])
DummyPhase = namedtuple("DummyPhase", ["name"])


class State(Node):
    """
    Simple class for attaching state to loops. Call the asdict method to get
    a nested dict-of-dict representation.
    """

    pass


@attr.s(auto_attribs=True)
class Metric:
    """
    An abstraction over the loop over minibatches.
    """

    name: str
    """name of the metric"""

    def compute(self, loop, phase):
        pass

    def reset(self):
        pass


def disconnect_all() -> None:
    """
    Disconnect all listeners from existing signals.
    """
    log.debug("disconnect_all called")
    for signal_name, signal in _callback_namespace.items():
        log.debug(f"disconnecting signals for {signal_name}")
        receivers = signal.receivers_for(blinker.ANY)
        for r in receivers:
            signal.disconnect(r)


def connect(
    signal: Union[str, blinker.Signal],
    callback: Union[Callable, "Callback"],
    sender: object = blinker.ANY,
    weak: bool = True,
) -> None:
    """
    Attach a listener to a signal specified either by name or object. If a sender is set,
    only trigger the callback if the sender matches the passed sender.

    Args:
        signal: a signal or str representing the target signal
        callback: the Callable to trigger when signal is sent
        sender (optional): only trigger the signal when sender matches passed object
        weak: if true (default) do not keep objects alive
    """
    if isinstance(signal, str):
        signal = _callback_namespace.signal(signal)

    log.debug(f"connecting signal {signal.name} to {callback}")
    signal.connect(callback, sender, weak)


def connect_via(
    signal: Union[str, blinker.Signal], sender: Any = blinker.ANY, weak: bool = True
):
    def decorator(callback):
        connect(signal, callback, sender, weak)
        return callback

    return decorator


@attr.s(auto_attribs=True)
class Callback(ABC):
    """
    A representation of any sort of callback
    """

    name: str = "Callback"
    """name of callback"""

    event_counter: int = 0
    """number of events this callback has seen"""

    @abstractmethod
    def process(self, sender, **kwargs) -> None:
        """

        Returns:

        """

    def trigger(self, sender, **kwargs) -> None:
        """
        Represents action to perform when event occurs.

        Args:
            sender: sender of the event
            **kwargs: additional parameters sent with the event
        """
        self.process(sender, **kwargs)
        self.event_counter += 1

    def reset(self, sender, **kwargs):
        """
        Reset signal

        Args:
            sender:
            **kwargs:

        Returns:

        """
        self.event_counter = 0

    @classmethod
    def simulate(cls, nevents, init_kwargs, sender, **kwargs):
        inst = cls(**init_kwargs)

        for i in range(nevents):
            inst.trigger(sender, **kwargs)

        return inst


@attr.s(auto_attribs=True)
class StatefulCallback(Callback):
    """
    Stores a history of calls
    """

    history: Optional[Sequence[Tuple[int, Any]]] = None

    @abstractmethod
    def process(self, sender, **kwargs) -> Any:
        pass

    def trigger(self, sender, **kwargs) -> None:
        if self.history is None:
            self.history = []

        value = self.process(sender, **kwargs)
        self.history.append((self.event_counter, value))

        self.event_counter += 1

    def reset(self, sender, **kwargs):
        super().reset(sender, **kwargs)
        self.history = []


@attr.s(auto_attribs=True)
class Phase:
    """
    An abstraction over the loop over minibatches.
    """

    name: str
    """name of the phase"""

    update_state: Callable
    """a callback to update the state of the model"""

    loader: Optional[Sequence] = None
    """an iterable containing the data for this phase"""

    metrics: Optional[Sequence[Metric]] = None
    """sequence of metrics to collect during this phase"""

    with_gradient: bool = True
    """compute gradients while running this phase"""

    phase_started = _callback_namespace.signal("phase_started")
    """started a phase"""

    iteration_started = _callback_namespace.signal("iteration_started")
    """signal emitted when an iteration is started"""

    iteration_completed = _callback_namespace.signal("iteration_completed")
    """signal emitted when an iteration is completed"""

    phase_completed = _callback_namespace.signal("phase_completed")
    """started a phase"""

    def run(self, loop: "Loop"):
        """
        Runs a single loop through the dataset
        """
        assert self.loader is not None, "must set loader to an iterable before run is called"

        output_path = "".join([self.name, ".output"])
        log.debug(f"Starting phase {self.name}")
        self.phase_started.send(self, loop=loop)

        for i, data in enumerate(self.loader):
            self.iteration_started.send(self, loop=loop)

            loop.state[output_path] = self.update_state(data, loop, loop.model)

            self.iteration_completed.send(self, loop=loop)

        log.debug(f"Completed phase {self.name}")
        self.phase_completed.send(self, loop=loop)


@attr.s(auto_attribs=True)
class Loop:
    """
    An abstraction representing the training loop.

    Attributes:

    """

    model: Any
    """model that we will be training"""

    loss_fn: Any
    """callable that will be evaluated by optimizers"""

    optimizers: Dict
    """optimizers"""

    phases: Dict[str, Phase]
    """sequence of phases to run"""

    callbacks: Optional[Dict[str, Callback]] = None
    """sequence of callbacks"""

    device: Optional[str] = None
    """device to use for placing data"""

    loop_started = _callback_namespace.signal(
        "loop_started", doc="emitted when the loop is started"
    )
    """signal emitted when loop is started"""

    epoch_started = _callback_namespace.signal("epoch_started")
    """signal emitted when loop is started"""

    epoch_completed = _callback_namespace.signal("epoch_completed")
    """loop complete"""

    loop_completed = _callback_namespace.signal("loop_completed")
    """loop completed"""

    state: State = attr.ib(factory=State)
    """an object representing the state"""

    def run(self, epochs):
        if epochs < 1:
            return

        if "epoch" not in self.state:
            self.state["epoch"] = 1

        epoch = self.state["epoch"]
        max_epochs = epoch + epochs - 1
        self.state["max_epochs"] = max_epochs
        log.debug(f"Running loop for {epochs} additional epochs. {epoch}/{max_epochs}")
        self.loop_started.send(self)

        try:
            while True:
                log.debug(f"Starting epoch {epoch}.")
                self.epoch_started.send(
                    self, epoch=epoch, max_epochs=self.state["max_epochs"]
                )

                for name, phase in self.phases.items():
                    phase.run(loop=self)

                log.debug(f"Completed epoch {epoch}.")
                self.epoch_completed.send(
                    self, epoch=epoch, max_epochs=self.state["max_epochs"]
                )
                self.state["epoch"] = epoch = epoch + 1

                if epoch > self.state["max_epochs"]:
                    break

        except Exception as e:
            # log exception here
            raise e

        self.loop_completed.send(self)
